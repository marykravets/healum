package com.healum.events;

import com.healum.main.ApiService;
import com.healum.main.RetrofitClient;
import com.healum.main.presentation.BaseFragment;

public class EventsTabFragment extends BaseFragment {

    @Override
    protected void getData() {
        ApiService api = RetrofitClient.getApiService();
        getData(api.getEvents());
        super.getData();
    }
}
