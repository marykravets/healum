package com.healum.messages.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message {

    @SerializedName("id")
    @Expose
    private String messageId;

    @SerializedName("date")
    @Expose
    private String messageDate;

    @SerializedName("title")
    @Expose
    private String messageTitle;

    @SerializedName("text")
    @Expose
    private String messageText;

    @SerializedName("image")
    @Expose
    private String messageImage;

    public String getMessageId() {
        return messageId;
    }

    public String getMessageDate() {
        return messageDate;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public String getMessageText() {
        return messageText;
    }

    public String getMessageImage() {
        return messageImage;
    }

    public void setMessageId(String value) {
        messageId = value;
    }

    public void setMessageDate(String value) {
        messageDate = value;
    }

    public void setMessageTitle(String value) {
        messageTitle = value;
    }

    public void setMessageText(String value) {
        messageText = value;
    }

    public void setMessageImage(String value) {
        messageImage = value;
    }
}
