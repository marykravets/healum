package com.healum.messages.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.healum.main.data.IData;

import java.util.ArrayList;

public class MessageList implements IData {

    @SerializedName("data")
    @Expose
    private ArrayList<Message> data = new ArrayList<>();

    @Override
    public ArrayList<Message> getData() {
        return data;
    }

    @Override
    public void setData(ArrayList<Message> data) {
        this.data = data;
    }
}
