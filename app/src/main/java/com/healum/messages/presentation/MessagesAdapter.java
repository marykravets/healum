package com.healum.messages.presentation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.healum.R;
import com.healum.messages.data.Message;

import java.util.List;

public class MessagesAdapter extends ArrayAdapter<Message> {

    private List<Message> mList;
    private Context mContext;
    private LayoutInflater mInflater;

    public MessagesAdapter(Context context, List<Message> objects) {
        super(context, 0, objects);
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mList = objects;
    }

    @Override
    public Message getItem(int position) {
        return mList.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.message_row, parent, false);
            holder = ViewHolder.create((RelativeLayout) view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Message item = getItem(position);

        holder.textViewName.setText(item != null ? item.getMessageTitle() : null);
        holder.textViewText.setText(item.getMessageText());
        Glide.with(mContext).load(item.getMessageImage()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(holder.imageView);

        return holder.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final ImageView imageView;
        public final TextView textViewName;
        public final TextView textViewText;

        private ViewHolder(RelativeLayout rootView, ImageView imageView, TextView textViewName, TextView textViewText) {
            this.rootView = rootView;
            this.imageView = imageView;
            this.textViewName = textViewName;
            this.textViewText = textViewText;
        }

        public static ViewHolder create(RelativeLayout rootView) {
            ImageView imageView = (ImageView) rootView.findViewById(R.id.imageView);
            TextView textViewName = (TextView) rootView.findViewById(R.id.msgTitle);
            TextView textViewText = (TextView) rootView.findViewById(R.id.msgText);
            return new ViewHolder(rootView, imageView, textViewName, textViewText);
        }
    }
}

