package com.healum.messages.presentation;

import com.healum.main.ApiService;
import com.healum.main.RetrofitClient;
import com.healum.main.presentation.BaseFragment;

public class MessagesTabFragment extends BaseFragment {

    @Override
    protected void getData() {
        ApiService api = RetrofitClient.getApiService();
        getData(api.getMessages());
        super.getData();
    }
}
