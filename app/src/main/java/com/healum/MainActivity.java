package com.healum;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.healum.all.SectionsPagerAdapter;
import com.healum.main.Const;
import com.healum.timeline.TimelineActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    private Unbinder unbinder;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    @BindView(R.id.container) ViewPager mViewPager;
    @BindView(R.id.main_tabs) TabLayout mTabAll;
    @BindView(R.id.toolbar) Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

        setToolbar();
        addListeners();
        initCustomTabs();
    }

    private void addListeners() {
        // Create the adapter that will return a fragment for each of the
        // primary sections of the activity.
        /*
      The {@link android.support.v4.view.PagerAdapter} that will provide
      fragments for each of the sections. We use a
      {@link FragmentPagerAdapter} derivative, which will keep every
      loaded fragment in memory. If this becomes too memory intensive, it
      may be best to switch to a
      {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), mTabAll.getTabCount());
        // Set up the ViewPager with the sections adapter.
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mTabAll.addOnTabSelectedListener(this);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabAll));
    }

    private void initCustomTabs() {
        mTabAll.setSelectedTabIndicatorHeight(Const.ZERO_HEIGHT);

        setCustomViewToTab(Const.ALL_PAGE_NUM, (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.tab_all, null));
        setCustomViewToTab(Const.MESSAGES_PAGE_NUM, (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.tab_messages, null));
        setCustomViewToTab(Const.EVENTS_PAGE_NUM, (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.tab_events, null));
    }

    private void setCustomViewToTab(int tabNum, RelativeLayout layout) {
        mTabAll.getTabAt(tabNum).setCustomView(layout);
    }

    private void setToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @OnClick(R.id.timeline_tab)
    public void onTimelineClick(View v) {
        Intent intent = new Intent(this, TimelineActivity.class);
        startActivity(intent);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        switch(tab.getPosition()) {
            case Const.ALL_PAGE_NUM:
                mViewPager.setCurrentItem(Const.ALL_PAGE_NUM);
                break;
            case Const.MESSAGES_PAGE_NUM:
                mViewPager.setCurrentItem(Const.MESSAGES_PAGE_NUM);
                break;
            default:
                mViewPager.setCurrentItem(Const.EVENTS_PAGE_NUM);
                break;
        }
    }

    @Override
    public void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
