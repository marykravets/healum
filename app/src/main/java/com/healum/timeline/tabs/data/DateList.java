package com.healum.timeline.tabs.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DateList {
    @SerializedName("data")
    @Expose
    private ArrayList<Date> data = new ArrayList<>();

    public ArrayList<Date> getData() {
        return data;
    }

    public void setData(ArrayList<Date> data) {
        this.data = data;
    }
}
