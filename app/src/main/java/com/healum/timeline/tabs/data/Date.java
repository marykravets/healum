package com.healum.timeline.tabs.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Date {

    @SerializedName("date")
    @Expose
    private String tabDate;

    public String getTabDate() {
        return tabDate;
    }

    public void setTabDate(String value) {
        tabDate = value;
    }
}