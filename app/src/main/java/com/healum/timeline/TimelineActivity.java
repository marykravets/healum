package com.healum.timeline;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.healum.R;
import com.healum.main.ApiService;
import com.healum.main.Const;
import com.healum.main.RetrofitClient;
import com.healum.timeline.card.data.ISwipe;
import com.healum.timeline.stack.data.Stack;
import com.healum.timeline.stack.data.presentation.ListStackAdapter;
import com.healum.timeline.stack.data.presentation.StackRecyclerView;
import com.healum.timeline.tabs.data.Date;
import com.healum.timeline.tabs.data.DateList;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TimelineActivity extends AppCompatActivity implements View.OnTouchListener, ISwipe {

    @BindView(R.id.chat_tab) RelativeLayout mChatTab;
    @BindView(R.id.chat_img) ImageView mChatImg;
    @BindView(R.id.timeline_tab) RelativeLayout mTimelineTab;
    @BindView(R.id.timeline_img) ImageView mTimelineImg;
    @BindView(R.id.timeline_tabs) TabLayout mTabLayout;
    @BindView(R.id.stacks_listview) StackRecyclerView mStacksListView;

    private ArrayList<Date> mDatesList;
    private List<Stack> mStackList;
    private Unbinder unbinder;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);
        unbinder = ButterKnife.bind(this);
        mContext = this;

        initUI();
        getData();
    }

    private void getData() {
        ApiService api = RetrofitClient.getApiService();

        getDateTabsData(api.getDates());
        getStacksData(api.getStacks());
    }

    private void initUI() {
        mChatTab.setBackgroundResource(R.drawable.inactive_white_left);
        mChatImg.setImageResource(R.drawable.chat_white_icon);

        mTimelineTab.setBackgroundResource(R.drawable.active_white_left);
        mTimelineImg.setImageResource(R.drawable.heart);
        mTabLayout.setSelectedTabIndicatorHeight(Const.ZERO_HEIGHT);

        mStacksListView.setLayoutManager(new LinearLayoutManager(this));
        mStacksListView.setHorizontalScrollBarEnabled(false);
        mStacksListView.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            mStacksListView.enableVerticalScroll(true);
        }

        return false;
    }

    @Override
    public void swipeInProgress() {
        mStacksListView.enableVerticalScroll(false);
    }

    @Override
    public void swipeFinished() {
        mStacksListView.enableVerticalScroll(true);
    }

    @OnClick(R.id.chat_tab)
    public void onChatClick(View v) {
       this.finish();
    }

    @Override
    public void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    private void getDateTabsData(Call<DateList> call) {
        call.enqueue(new Callback<DateList>() {
            @Override
            public void onResponse(Call<DateList> call, Response<DateList> response) {
                if (response.isSuccessful()) {
                    mDatesList = response.body().getData();
                    addDateTabs();
                } else {
                    Snackbar.make(mTabLayout, R.string.string_something_wrong, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<DateList> call, Throwable t) {
                Snackbar.make(mTabLayout, R.string.string_something_wrong, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void getStacksData(Call<List<Stack>> call) {
        mStackList = new ArrayList<>();
        call.enqueue(new Callback<List<Stack>>() {
            @Override
            public void onResponse(Call<List<Stack>> call, Response<List<Stack>> response) {
                if (response.isSuccessful()) {
                    for(int i = 0; i < response.body().size(); i++) {
                        mStackList.add(i, response.body().get(i));
                    }

                    if( mStacksListView != null ) {
                        mStacksListView.setAdapter(new ListStackAdapter(mContext, mStackList));
                    }
                } else {
                    Snackbar.make(mTabLayout, R.string.string_something_wrong, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<Stack>> call, Throwable t) {
                Snackbar.make(mTabLayout, R.string.string_something_wrong, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void addDateTabs() {
        TabLayout.Tab tab;
        LinearLayout layout;
        for (int i = 0; i < mDatesList.size(); i++) {
            tab = mTabLayout.newTab();
            layout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.tab_date, null);
            ((TextView)layout.findViewById(R.id.date_text)).setText(mDatesList.get(i).getTabDate());
            tab.setCustomView(layout);
            mTabLayout.addTab(tab);
        }
    }
}
