package com.healum.timeline.stack.data.presentation;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class StackRecyclerView extends RecyclerView {

    private boolean verticleScrollingEnabled = true;

    public void enableVerticalScroll(boolean enabled) {
        verticleScrollingEnabled = enabled;
    }

    public boolean isVerticalScrollingEnabled() {
        return verticleScrollingEnabled;
    }

    @Override
    public int computeVerticalScrollRange() {

        if (isVerticalScrollingEnabled())
            return super.computeVerticalScrollRange();
        return 0;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent e) {

        if (isVerticalScrollingEnabled())
            return super.onInterceptTouchEvent(e);
        return false;

    }

    public StackRecyclerView(Context context) {
        super(context);
    }

    public StackRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public StackRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}