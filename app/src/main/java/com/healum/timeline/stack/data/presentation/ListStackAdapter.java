package com.healum.timeline.stack.data.presentation;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.healum.R;
import com.healum.main.Const;
import com.healum.timeline.card.data.ISwipe;
import com.healum.timeline.card.presentation.Card;
import com.healum.timeline.stack.data.Stack;
import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;

import java.util.ArrayList;
import java.util.List;

public class ListStackAdapter extends RecyclerView.Adapter<ListStackAdapter.ViewHolder> {

    private List<Stack> mList;
    private LayoutInflater mInflater;
    private Context mContext;

    public ListStackAdapter(Context context, List<Stack> objects) {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mList = objects;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.stack_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        holder.rootView.setHorizontalScrollBarEnabled(false);
        holder.rootView.getBuilder().setSwipeType(SwipePlaceHolderView.SWIPE_TYPE_HORIZONTAL)
                .setDisplayViewCount(Const.DISPLAY_VIEW_COUNT)
                .setSwipeDecor(new SwipeDecor()
                        .setSwipeDistToDisplayMsg(Const.CARD_SWIPE_DIST)
                        .setPaddingTop(Const.CARD_PADDING_TOP)
                        .setRelativeScale(Const.CARD_RELATIVE_SCALE)
                        .setSwipeAnimTime(Const.SWIPE_ANIMATION_TIME));

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ArrayList<com.healum.timeline.stack.data.Card> cards = mList.get(position).getCards();
        ISwipe swipeListener = (ISwipe) mContext;
        for (int i = 0; i < cards.size(); i++) {
            holder.rootView.addView(new Card(swipeListener, holder.rootView, cards.get(i)));
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final SwipePlaceHolderView rootView;

        public ViewHolder(View view) {
            super(view);
            this.rootView = (SwipePlaceHolderView)view.findViewById(R.id.stackView);
        }
    }
}


