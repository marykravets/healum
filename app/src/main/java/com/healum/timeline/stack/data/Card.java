package com.healum.timeline.stack.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Card {

    @SerializedName("id_card")
    @Expose
    private String idCard;

    @SerializedName("color")
    @Expose
    private String cardColor;

    @SerializedName("name")
    @Expose
    private String cardName;

    @SerializedName("description")
    @Expose
    private String cardDescription;

    @SerializedName("card_column1_text")
    @Expose
    private String cardCol1Text;

    @SerializedName("card_column1_value")
    @Expose
    private String cardCol1Value;

    @SerializedName("card_column2_text")
    @Expose
    private String cardCol2Text;

    @SerializedName("card_column2_value")
    @Expose
    private String cardCol2Value;

    @SerializedName("card_column3_text")
    @Expose
    private String cardCol3Text;

    @SerializedName("card_column3_value")
    @Expose
    private String cardCol3Value;

    @SerializedName("likes")
    @Expose
    private String cardLikes;

    @SerializedName("xp")
    @Expose
    private String cardXP;

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String value) {
        idCard = value;
    }

    public String getCardColor() {
        return cardColor;
    }

    public void setCardColor(String value) {
        cardColor = value;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String value) {
        cardName = value;
    }

    public String getCardDescription() {
        return cardDescription;
    }

    public void setCardDescription(String value) {
        cardDescription = value;
    }

    public String getCardCol1Text() {
        return cardCol1Text;
    }

    public void setCardCol1Text(String value) {
        cardCol1Text = value;
    }

    public String getCardCol1Value() {
        return cardCol1Value;
    }

    public void setCardCol1Value(String value) {
        cardCol1Value = value;
    }

    public String getCardCol2Text() {
        return cardCol2Text;
    }

    public void setCardCol2Text(String value) {
        cardCol2Text = value;
    }

    public String getCardCol2Value() {
        return cardCol2Value;
    }

    public void setCardCol2Value(String value) {
        cardCol2Value = value;
    }

    public String getCardCol3Text() {
        return cardCol3Text;
    }

    public void setCardCol3Text(String value) {
        cardCol3Text = value;
    }

    public String getCardCol3Value() {
        return cardCol3Value;
    }

    public void setCardCol3Value(String value) {
        cardCol3Value = value;
    }

    public String getCardLikes() {
        return cardLikes;
    }

    public void setCardLikes(String value) {
        cardLikes = value;
    }

    public String getCardXP() {
        return cardXP;
    }

    public void setCardXP(String value) {
        cardXP = value;
    }
}
