package com.healum.timeline.card.presentation;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.widget.TextView;

import com.healum.R;
import com.healum.main.Const;
import com.healum.timeline.card.data.ISwipe;
import com.mindorks.placeholderview.Animation;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.annotations.Animate;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.NonReusable;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeIn;
import com.mindorks.placeholderview.annotations.swipe.SwipeInState;
import com.mindorks.placeholderview.annotations.swipe.SwipeOut;
import com.mindorks.placeholderview.annotations.swipe.SwipeOutState;

@Animate(Animation.ENTER_LEFT_DESC)
@NonReusable
@Layout(R.layout.card_view)
public class Card implements android.view.View.OnClickListener {

    @View(R.id.calories)
    private TextView mCaloriesView;

    @View(R.id.hour)
    private TextView mHourView;

    @View(R.id.steps)
    private TextView mStepsView;

    @View(R.id.xp)
    private TextView mXPView;

    @View(R.id.likes_count)
    private TextView mLikesCountView;

    @View(R.id.card_name)
    private TextView mCardName;

    @View(R.id.card_description)
    private TextView mCardDescription;

    @View(R.id.cardView)
    private CardView mCardView;

    private com.healum.timeline.stack.data.Card mCard;
    private Context mContext;
    private ISwipe swipeListener;

    public Card(ISwipe listener, SwipePlaceHolderView holderView, com.healum.timeline.stack.data.Card card) {
        mCard = card;
        swipeListener = listener;
        mContext = holderView.getContext();
    }

    @SwipeOutState
    private void onSwipeOutState() {
        swipeListener.swipeInProgress();
    }

    @SwipeInState
    private void onSwipeInState() {
        swipeListener.swipeInProgress();
    }

    @SwipeOut
    private void onSwipeOut() {
        swipeListener.swipeFinished();
    }

    @SwipeIn
    private void onSwipeIn() {
        swipeListener.swipeFinished();
    }

    @Resolve
    private void onResolved() {
        mCardView.setOnClickListener(this);
        CharSequence calorieText = Html.fromHtml(String.format(mContext.getResources().getString(R.string.twolined_title), mCard.getCardCol1Value(), mCard.getCardCol1Text()));
        mCaloriesView.setText(calorieText);
        CharSequence hourText = Html.fromHtml(String.format(mContext.getResources().getString(R.string.twolined_title), mCard.getCardCol2Value(), mCard.getCardCol2Text()));
        mHourView.setText(hourText);
        CharSequence stepsText = Html.fromHtml(String.format(mContext.getResources().getString(R.string.twolined_title), mCard.getCardCol3Value(), mCard.getCardCol3Text()));
        mStepsView.setText(stepsText);
        CharSequence xpText = Html.fromHtml(String.format(mContext.getResources().getString(R.string.xp), mCard.getCardXP()));
        mXPView.setText(xpText);
        mXPView.setTextColor(Color.parseColor(mCard.getCardColor()));
        mLikesCountView.setText(String.format(mContext.getResources().getString(R.string.likes_text), mCard.getCardLikes()));
        mCardName.setText(mCard.getCardName());
        mCardDescription.setText(mCard.getCardDescription());
        mCardView.setCardBackgroundColor(Color.parseColor(mCard.getCardColor()));
    }

    @Override
    public void onClick(android.view.View v) {
        Intent intent = new Intent(mContext, CardDetailActivity.class);
        Bundle b = new Bundle();
        b.putString(Const.CARD_ID, mCard.getIdCard());
        intent.putExtras(b);
        mContext.startActivity(intent);
    }
}