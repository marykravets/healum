package com.healum.timeline.card.presentation;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.healum.R;
import com.healum.main.ApiService;
import com.healum.main.Const;
import com.healum.main.RetrofitClient;
import com.healum.timeline.card.presentation.content.ContentFragment;
import com.healum.timeline.card.presentation.content.ExpertsFragment;
import com.healum.timeline.card.presentation.content.OrderFragment;
import com.healum.timeline.card.tabs.data.CardTab;
import com.healum.timeline.card.tabs.data.CardTabList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CardTabsFragment extends CardBaseFragment implements TabLayout.OnTabSelectedListener {

    @BindView(R.id.detail_tabs) TabLayout mTabLayout;
    @BindView(R.id.tab_container) HorizontalViewPager mViewPager;
    private ArrayList<CardTab> mTabsList;
    private ImageView image;
    private TabsPagerAdapter mPagerAdapter;

    public CardTabsFragment() {

    }

    public static Fragment newInstance(int position) {
        Bundle args = new Bundle();
        args.putInt("position", position);
        CardTabsFragment fragment = new CardTabsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card_detail_tabs, container, false);
        ButterKnife.bind(this, view);
        getData();

        return view;
    }

    private void initTabLayout() {
        mPagerAdapter = new TabsPagerAdapter(getChildFragmentManager(), mTabLayout.getTabCount());

        for (int i = 0; i < mTabsList.size()-1; i++) {
            if (i == 3) {
                mPagerAdapter.addFragment(new OrderFragment());
                continue;
            }
            addTab(i);
        }
        mPagerAdapter.addFragment(new ExpertsFragment());

        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mViewPager.setOverScrollMode(View.OVER_SCROLL_NEVER);
        mViewPager.setVerticalScrollBarEnabled(false);
        mTabLayout.setSelectedTabIndicatorHeight(Const.ZERO_HEIGHT);
        mTabLayout.addOnTabSelectedListener(this);
    }

    private void addTab(int i) {
        ContentFragment tab = new ContentFragment();
        setBundle(tab, mTabsList.get(i).getTabUrl());
        mPagerAdapter.addFragment(tab);
    }

    private void setBundle(ContentFragment fragment, String tabUrl) {
        Bundle args = new Bundle();
        args.putString(Const.URL, tabUrl);
        fragment.setArguments(args);
    }

    private void getData() {
        ApiService api = RetrofitClient.getApiService();
        getTabsData(api.getCardTabs());
    }

    private void getTabsData(Call<CardTabList> call) {
        call.enqueue(new Callback<CardTabList>() {
            @Override
            public void onResponse(Call<CardTabList> call, Response<CardTabList> response) {
                if (response.isSuccessful()) {
                    mTabsList = response.body().getData();
                    addTabs();
                    initTabLayout();
                } else {
                    Snackbar.make(mTabLayout, R.string.string_something_wrong, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CardTabList> call, Throwable t) {
                Snackbar.make(mTabLayout, R.string.string_something_wrong, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void addTabs() {
        TabLayout.Tab tab;
        LinearLayout layout;
        for (int i = 0; i < mTabsList.size(); i++) {
            tab = mTabLayout.newTab();
            layout = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.tab_card_detail, null);
            ((TextView)layout.findViewById(R.id.tab_text)).setText(mTabsList.get(i).getTabName());
            image = (ImageView)layout.findViewById(R.id.tab_imageview);

            if (i == 0) {
                Glide.with(getActivity()).load(mTabsList.get(i).getTabSelectedImage()).into(image);
            } else {
                Glide.with(getActivity()).load(mTabsList.get(i).getTabImage()).into(image);
            }

            tab.setCustomView(layout);
            mTabLayout.addTab(tab);
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        image = (ImageView)tab.getCustomView().findViewById(R.id.tab_imageview);

        if (image != null) {
            Glide.with(getActivity()).load(mTabsList.get(tab.getPosition()).getTabSelectedImage()).into(image);
        }

        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        image = (ImageView)tab.getCustomView().findViewById(R.id.tab_imageview);

        if (image != null) {
            Glide.with(getActivity()).load(mTabsList.get(tab.getPosition()).getTabImage()).into(image);
        }
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
