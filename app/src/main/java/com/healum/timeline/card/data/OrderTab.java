package com.healum.timeline.card.data;

import com.google.gson.annotations.SerializedName;

public class OrderTab {

    @SerializedName("url")
    public String url;

    @SerializedName("title")
    public String title;

    @SerializedName("description")
    public String description;
}
