package com.healum.timeline.card.data;

import com.google.gson.annotations.SerializedName;

public class ExpertsTab {

    @SerializedName("title")
    public String title;

    @SerializedName("description")
    public String description;
}
