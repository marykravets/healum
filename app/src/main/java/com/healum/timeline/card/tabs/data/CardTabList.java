package com.healum.timeline.card.tabs.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CardTabList {

    @SerializedName("data")
    @Expose
    private ArrayList<CardTab> data = new ArrayList<>();

    public ArrayList<CardTab> getData() {
        return data;
    }

    public void setData(ArrayList<CardTab> data) {
        this.data = data;
    }
}

