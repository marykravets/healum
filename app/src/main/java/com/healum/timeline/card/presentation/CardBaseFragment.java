package com.healum.timeline.card.presentation;

import android.support.v4.app.Fragment;

import com.healum.timeline.card.data.ExpertsTab;
import com.healum.timeline.card.data.NutritionTab;
import com.healum.timeline.card.data.OrderTab;

public class CardBaseFragment extends Fragment {
    protected static NutritionTab mNutritionTab;
    protected static OrderTab mOrderTab;
    protected static ExpertsTab mExpertsTab;
}
