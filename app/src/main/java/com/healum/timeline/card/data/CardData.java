package com.healum.timeline.card.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CardData {
    @SerializedName("url")
    @Expose
    private String cardImgUrl;

    @SerializedName("title")
    @Expose
    private String cardTitle;

    @SerializedName("detail")
    @Expose
    private String cardDetail;

    @SerializedName("xp")
    @Expose
    private String cardXp;

    @SerializedName("fotos")
    @Expose
    private String cardFotos;

    @SerializedName("likes")
    @Expose
    private String cardLikes;

    @SerializedName("comments")
    @Expose
    private String cardComments;

    @SerializedName("shares")
    @Expose
    private String cardShares;

    @SerializedName("nutrition")
    private NutritionTab cardNutrition;

    @SerializedName("order")
    private OrderTab cardOrder;

    @SerializedName("experts")
    private ExpertsTab cardExperts;

    public String getCardImgUrl() {
        return cardImgUrl;
    }

    public void setCardImgUrl(String value) {
        cardImgUrl = value;
    }

    public String getCardTitle() {
        return cardTitle;
    }

    public void setCardTitle(String value) {
        cardTitle = value;
    }

    public String getCardDetail() {
        return cardDetail;
    }

    public void setCardDetail(String value) {
        cardDetail = value;
    }

    public String getCardXp() {
        return cardXp;
    }

    public void setCardXp(String value) {
        cardXp = value;
    }

    public String getCardFotos() {
        return cardFotos;
    }

    public void setCardFotos(String value) {
        cardFotos = value;
    }

    public String getCardLikes() {
        return cardLikes;
    }

    public void setCardLikes(String value) {
        cardLikes = value;
    }

    public String getCardComments() {
        return cardComments;
    }

    public void setCardComments(String value) {
        cardComments = value;
    }

    public String getCardShares() {
        return cardShares;
    }

    public void setCardShares(String value) {
        cardShares = value;
    }

    public NutritionTab getNutritionTab() {
        return cardNutrition;
    }

    public void setNutritionTab(NutritionTab value) {
        cardNutrition = value;
    }

    public OrderTab getOrderTab() {
        return cardOrder;
    }

    public ExpertsTab getExpertsTab() {
        return cardExperts;
    }
}
