package com.healum.timeline.card.data;

import com.google.gson.annotations.SerializedName;

public class NutritionTab {
    @SerializedName("url")
    public String url;

    @SerializedName("Nutrition")
    public Nutrition nutrition;

    @SerializedName("kcal")
    public String kcal;

    @SerializedName("protein")
    public String protein;

    @SerializedName("Karbs")
    public String karbs;

    @SerializedName("Fiber")
    public String fiber;

    @SerializedName("Sugars")
    public String sugars;

    @SerializedName("Fat")
    public String fat;

    @SerializedName("Saturated Fat")
    public String saturated_fat;

    @SerializedName("Unsaturated Fat")
    public String unsaturated_fat;
}
