package com.healum.timeline.card.presentation.content;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.healum.R;
import com.healum.main.Const;
import com.healum.timeline.card.presentation.CardBaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContentFragment extends CardBaseFragment {

    @BindView(R.id.webView)
    WebView mWebView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_nutrition, container, false);
        ButterKnife.bind(this, rootView);

        Bundle b = getArguments();
        String url = b.getString(Const.URL);

        mWebView.setHorizontalScrollBarEnabled(false);
        mWebView.loadUrl(url);

        return rootView;
    }

    @OnClick(R.id.add_plan)
    public void addToPlanClick() {
        Snackbar.make(mWebView, R.string.adding, Snackbar.LENGTH_SHORT).show();
    }
}
