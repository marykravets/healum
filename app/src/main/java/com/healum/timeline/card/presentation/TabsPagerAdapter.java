package com.healum.timeline.card.presentation;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class TabsPagerAdapter extends FragmentPagerAdapter {

    private int mPagesNum;
    private final List<Fragment> mFragmentList = new ArrayList<>();

    public TabsPagerAdapter(FragmentManager fm, int pagesNum) {
        super(fm);
        mPagesNum = pagesNum;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    public void addFragment(Fragment fragment) {
        mFragmentList.add(fragment);
    }

    @Override
    public int getCount() {
        return mPagesNum;
    }
}