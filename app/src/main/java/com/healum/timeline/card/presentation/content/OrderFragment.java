package com.healum.timeline.card.presentation.content;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.healum.R;
import com.healum.timeline.card.presentation.CardBaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderFragment extends CardBaseFragment {

    @BindView(R.id.image) ImageView mImage;
    @BindView(R.id.title) TextView mTitle;
    @BindView(R.id.description) TextView mDescription;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_order, container, false);
        ButterKnife.bind(this, rootView);

        if (mOrderTab != null) {
            Glide.with(getActivity()).load(mOrderTab.url).error(R.drawable.photo).into(mImage);
            mTitle.setText(mOrderTab.title);
            mDescription.setText(mOrderTab.description);
        }

        return rootView;
    }
}
