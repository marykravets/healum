package com.healum.timeline.card.tabs.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CardTab {

    @SerializedName("id")
    @Expose
    private String tabId;

    @SerializedName("name")
    @Expose
    private String tabName;

    @SerializedName("img")
    @Expose
    private String tabImage;

    @SerializedName("img_selected")
    @Expose
    private String tabSelectedImage;

    @SerializedName("url")
    @Expose
    private String tabUrl;

    public String getTabId() {
        return tabId;
    }

    public void setTabId(String value) {
        tabId = value;
    }

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String value) {
        tabName = value;
    }

    public String getTabImage() {
        return tabImage;
    }

    public void setTabImage(String value) {
        tabImage = value;
    }

    public String getTabSelectedImage() {
        return tabSelectedImage;
    }

    public void setTabSelectedImage(String value) {
        tabSelectedImage = value;
    }

    public String getTabUrl() {
        return tabUrl;
    }

    public void setTabUrl(String value) {
        tabUrl = value;
    }
}

