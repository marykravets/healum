package com.healum.timeline.card.presentation;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.healum.R;
import com.healum.main.ApiService;
import com.healum.main.Const;
import com.healum.main.RetrofitClient;
import com.healum.timeline.card.data.CardData;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CardImageFragment extends CardBaseFragment implements View.OnClickListener {

    @BindView(R.id.card_detail_image) ImageView mImage;
    @BindView(R.id.back_btn) Button mBackBtn;
    @BindView(R.id.big_title) TextView mBigTitle;
    @BindView(R.id.description) TextView mDescription;
    @BindView(R.id.xp) TextView mXp;
    @BindView(R.id.camera_num) TextView mCameraNum;
    @BindView(R.id.heart_num) TextView mHeartNum;
    @BindView(R.id.comment_num) TextView mCommentNum;
    @BindView(R.id.share_num) TextView mShareNum;

    public interface OnCardImageFragmentCreatedListener {
        void onCardImageFragmentCreated(View v);
    }

    public CardImageFragment() {

    }

    public static Fragment newInstance(int position, String cardId) {
        Bundle args = new Bundle();
        args.putInt("position", position);
        args.putString(Const.CARD_ID, cardId);
        CardImageFragment fragment = new CardImageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card_detail_image, container, false);
        ButterKnife.bind(this, view);

        mBackBtn.setOnClickListener(this);

        OnCardImageFragmentCreatedListener mListener = (OnCardImageFragmentCreatedListener) getActivity();
        mListener.onCardImageFragmentCreated(view);
        getData();
        return view;
    }

    private void getData() {
        ApiService api = RetrofitClient.getApiService();
        getCardData(api.getCardDetail(getArguments().getString(Const.CARD_ID)+Const.CARD_FILE));
    }

    private void getCardData(Call<CardData> call) {
        call.enqueue(new Callback<CardData>() {
            @Override
            public void onResponse(Call<CardData> call, Response<CardData> response) {
                if (response.isSuccessful()) {
                    CardData data = response.body();
                    Glide.with(getActivity()).load(data.getCardImgUrl()).error(R.drawable.photo).into(mImage);
                    mBigTitle.setText(data.getCardTitle());
                    mDescription.setText(data.getCardDetail());
                    mXp.setText(Html.fromHtml(String.format(getActivity().getResources().getString(R.string.xp), data.getCardXp())));
                    mCameraNum.setText(data.getCardFotos());
                    mHeartNum.setText(data.getCardLikes());
                    mCommentNum.setText(data.getCardComments());
                    mShareNum.setText(data.getCardShares());

                    mNutritionTab = data.getNutritionTab();
                    mOrderTab = data.getOrderTab();
                    mExpertsTab = data.getExpertsTab();
                } else {
                    Snackbar.make(mImage, R.string.string_something_wrong, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CardData> call, Throwable t) {
                Snackbar.make(mImage, R.string.string_something_wrong, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        getActivity().finish();
    }
}
