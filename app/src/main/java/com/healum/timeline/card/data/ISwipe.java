package com.healum.timeline.card.data;

public interface ISwipe {
    void swipeInProgress();
    void swipeFinished();
}
