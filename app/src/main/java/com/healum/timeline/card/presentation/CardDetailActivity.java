package com.healum.timeline.card.presentation;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.healum.R;
import com.healum.main.Const;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.kaelaela.verticalviewpager.VerticalViewPager;

public class CardDetailActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, CardImageFragment.OnCardImageFragmentCreatedListener, Animation.AnimationListener {

    @BindView(R.id.vertical_viewpager) VerticalViewPager mViewPager;
    private Unbinder unbinder;
    private ImageView mCameraBig;
    private int mPageSelected = Const.IMAGE_FRAGMENT_NUM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_card_detail);
        unbinder = ButterKnife.bind(this);

        initUI();
    }

    private void initUI() {
        mViewPager.setAdapter(new ContentFragmentAdapter.Holder(getSupportFragmentManager())
                        .add(CardImageFragment.newInstance(Const.IMAGE_FRAGMENT_NUM, getIntent().getExtras().getString(Const.CARD_ID)))
                        .add(CardTabsFragment.newInstance(Const.TABS_FRAGMENT_NUM)).set());
        mViewPager.setOverScrollMode(View.OVER_SCROLL_NEVER);
        mViewPager.addOnPageChangeListener(this);
    }

    @Override
    public void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        mPageSelected = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (state == ViewPager.SCROLL_STATE_DRAGGING) {
            if (mPageSelected == Const.IMAGE_FRAGMENT_NUM) {
                animate(R.anim.card_image_scale_animation);
            } else {
                animate(R.anim.card_image_scale_back_animation);
            }
        }
    }

    private void animate(int animation) {
        Animation scale = AnimationUtils.loadAnimation(this, animation);
        scale.setAnimationListener(this);
        AnimationSet animSet = new AnimationSet(true);
        animSet.setFillEnabled(false);
        animSet.addAnimation(scale);
        mCameraBig.startAnimation(animSet);
    }

    @Override
    public void onCardImageFragmentCreated(View v) {
        mCameraBig = (ImageView) v.findViewById(R.id.camera_big);
    }

    @Override
    public void onAnimationStart(Animation animation) {
        mCameraBig.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (mPageSelected == Const.IMAGE_FRAGMENT_NUM) {
            mCameraBig.setVisibility(View.VISIBLE);
        } else {
            mCameraBig.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
