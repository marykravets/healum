package com.healum.timeline.card.data;

import com.google.gson.annotations.SerializedName;

public class Nutrition {
    @SerializedName("Kcal")
    public String kcal;

    @SerializedName("Carbs")
    public String carbs;

    @SerializedName("Fat")
    public String fat;

    @SerializedName("Protein")
    public String protein;
}
