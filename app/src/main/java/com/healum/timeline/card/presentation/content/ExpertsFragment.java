package com.healum.timeline.card.presentation.content;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.healum.R;
import com.healum.timeline.card.presentation.CardBaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExpertsFragment extends CardBaseFragment {

    @BindView(R.id.title) TextView mTitle;
    @BindView(R.id.description) TextView mDescription;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_experts, container, false);
        ButterKnife.bind(this, rootView);

        mTitle.setText(mExpertsTab.title);
        mDescription.setText(mExpertsTab.description);

        return rootView;
    }
}
