package com.healum.all;

import com.healum.main.ApiService;
import com.healum.main.RetrofitClient;
import com.healum.main.presentation.BaseFragment;

public class AllTabFragment extends BaseFragment {

    @Override
    protected void getData() {
        ApiService api = RetrofitClient.getApiService();
        getData(api.getAllMessages());
        super.getData();
    }
}
