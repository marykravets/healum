package com.healum.all;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.healum.events.EventsTabFragment;
import com.healum.main.Const;
import com.healum.messages.presentation.MessagesTabFragment;

public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    private int mPagesNum;

    public SectionsPagerAdapter(FragmentManager fm, int pagesNum) {
        super(fm);
        mPagesNum = pagesNum;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case Const.ALL_PAGE_NUM:
                return new AllTabFragment();
            case Const.MESSAGES_PAGE_NUM:
                return new MessagesTabFragment();
            case Const.EVENTS_PAGE_NUM:
                return new EventsTabFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mPagesNum;
    }
}