package com.healum.main;

import com.healum.messages.data.MessageList;
import com.healum.timeline.card.data.CardData;
import com.healum.timeline.card.tabs.data.CardTabList;
import com.healum.timeline.stack.data.Stack;
import com.healum.timeline.tabs.data.DateList;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {
    @GET("/messages.php")
    Call<MessageList> getMessages();

    @GET("/messages-all.php")
    Call<MessageList> getAllMessages();

    @GET("/events.php")
    Call<MessageList> getEvents();

    @GET("timeline/dates.php")
    Call<DateList> getDates();

    @GET("card_detail/tabs/tabs.php")
    Call<CardTabList> getCardTabs();

    @GET("card_detail/{fileName}")
    Call<CardData> getCardDetail(@Path("fileName") String fileName);

    @GET("stacks.php")
    Call<List<Stack>> getStacks();
}
