package com.healum.main;

public class Const {
    public static final int ALL_PAGE_NUM = 0;
    public static final int MESSAGES_PAGE_NUM = 1;
    public static final int EVENTS_PAGE_NUM = 2;

    public static final int ZERO_HEIGHT = 0;

    public static final int DISPLAY_VIEW_COUNT = 3;
    public static final int SWIPE_ANIMATION_TIME = 120;
    public static final float CARD_RELATIVE_SCALE = 0.01f;
    public static final int CARD_PADDING_TOP = 20;
    public static final int CARD_SWIPE_DIST= 10;

    public static final int IMAGE_FRAGMENT_NUM = 0;
    public static final int TABS_FRAGMENT_NUM = 0;

    public static final String CARD_ID = "cardId";
    public static final String CARD_FILE = "_card_detail.php";

    public static final String URL = "url";
}
