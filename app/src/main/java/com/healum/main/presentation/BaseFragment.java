package com.healum.main.presentation;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.healum.R;
import com.healum.messages.data.Message;
import com.healum.messages.data.MessageList;
import com.healum.messages.presentation.MessagesAdapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private ListView mList;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private MessagesAdapter adapter;
    private ArrayList<Message> contactList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        findViews(rootView);

        getData();
        addFloatingButton(rootView);

        return rootView;
    }

    private void findViews(View rootView) {
        mList = (ListView)rootView.findViewById(R.id.listView);
        mSwipeRefreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    protected void getData() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void addFloatingButton(View rootView) {
        Button fab = (Button)rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    protected void getData(Call<MessageList> call) {
        call.enqueue(new Callback<MessageList>() {
            @Override
            public void onResponse(Call<MessageList> call, Response<MessageList> response) {
                if (response.isSuccessful()) {
                    contactList = response.body().getData();
                    Activity activity = getActivity();

                    if (activity != null) {
                        adapter = new MessagesAdapter(activity, contactList);
                        mList.setAdapter(adapter);
                    }
                } else {
                    Snackbar.make(mList, R.string.string_something_wrong, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<MessageList> call, Throwable t) {
                Snackbar.make(mList, R.string.string_something_wrong, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onRefresh() {
        getData();
    }
}
