package com.healum.main.data;

import com.healum.messages.data.Message;

import java.util.ArrayList;

public interface IData {
    ArrayList<Message> getData();
    void setData(ArrayList<Message> data);
}
